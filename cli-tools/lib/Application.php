<?php

namespace lib;

/**
 * Class Application
 */
class Application
{
	var $arguments = NULL;

	var $debug = 0;

	public function __construct ($argv)
	{
		if ($argv)
			$this->arguments = parseArgs($argv);

		/*
		$this->serverCfg = $this->loadCfgFile('/etc/shipard-node/server.json');
		if (!$this->serverCfg)
			$this->err ("File /etc/shipard-node/server.json not found or has syntax error!");

		$this->nodeCfg = $this->loadCfgFile('/etc/shipard-node/config.json');
*/
		if ($this->arg('debug'))
		{
			$this->debug = intval($this->arg('debug'));
			echo "-- debug mode: {$this->debug}\n";
		}
	}

	public function arg ($name)
	{
		if (isset ($this->arguments [$name]))
			return strval($this->arguments [$name]);

		return FALSE;
	}

	public function apiCall ($url)
	{
		$opts = array(
			'http'=>array(
				'timeout' => 30, 'method'=>"GET",
				'header'=>
					"e10-api-key: " . $this->serverCfg['apiKey'] . "\r\n".
					"e10-device-id: " . $this->machineDeviceId (). "\r\n".
					"Connection: close\r\n"
			)
		);
		$context = stream_context_create($opts);

		$resultCode = file_get_contents ($url, FALSE, $context);

		$resultData = json_decode ($resultCode, TRUE);
		return $resultData;
	}

	public function apiSend ($url, $requestData)
	{
		$requestDataStr = json_encode($requestData);

		$opts = array(
			'http'=>array(
				'timeout' => 30,
				'method'=>"GET",
				'header'=>
					"e10-api-key: " . $this->serverCfg['apiKey'] . "\r\n".
					"e10-device-id: " . $this->machineDeviceId (). "\r\n".
					"Content-type: text/json"."\r\n".
					"Content-Length: " . strlen($requestDataStr). "\r\n".
					"Connection: close\r\n",
				'content' => $requestDataStr,
			)
		);
		$context = stream_context_create($opts);

		$resultCode = file_get_contents ($url, FALSE, $context);
		$resultData = json_decode ($resultCode, TRUE);

		return $resultData;
	}

	public function cfgItem ($cfg, $key, $defaultValue = NULL)
	{
		if (isset ($cfg [$key]))
			return $cfg [$key];

		$parts = explode ('.', $key);
		if (!count ($parts))
			return $defaultValue;

		$value = NULL;
		$top = $cfg;
		forEach ($parts as $p)
		{
			if (isset ($top [$p]))
			{
				$value = &$top [$p];
				$top = &$top [$p];
				continue;
			}
			return $defaultValue;
		}

		return $value;
	}

	public function command ($idx = 0)
	{
		if (isset ($this->arguments [$idx]))
			return $this->arguments [$idx];

		return "";
	}

	public function err ($msg)
	{
		if ($msg === FALSE)
			return TRUE;

		if (is_array($msg))
		{
			if (count($msg) !== 0)
			{
				forEach ($msg as $m)
					echo ("! " . $m['text']."\n");
				return FALSE;
			}
			return TRUE;
		}

		echo ("ERROR: ".$msg."\n");
		return FALSE;
	}

	public function loadCfgFile ($fileName)
	{
		if (is_file ($fileName))
		{
			$cfgString = file_get_contents ($fileName);
			if (!$cfgString)
				return FALSE;
			$cfg = json_decode ($cfgString, true);
			if (!$cfg)
				return FALSE;
			return $cfg;
		}
		return FALSE;
	}

	public function machineDeviceId ()
	{
		if (!is_file('/etc/e10-device-id.cfg'))
		{
			$deviceId = md5(json_encode(posix_uname()).mt_rand (1000000, 999999999).'-'.time().'-'.mt_rand (1000000, 999999999));
			file_put_contents('/etc/e10-device-id.cfg', $deviceId);
		}
		else
		{
			$deviceId = file_get_contents('/etc/e10-device-id.cfg');
		}

		return $deviceId;
	}

	public function initUploadInfo ($type, $table)
	{
		$shnCfg = $this->loadCfgFile('/etc/shipard-node/server.json');
		if ($shnCfg)
		{
			$dir = '/var/lib/shipard-node/upload/' . $type;
			if (!is_dir($dir))
				mkdir($dir, 0770, TRUE);

			$settingsFileName = $dir . '/.settings';
			if (!is_file($settingsFileName)) {
				$settingsData = ['dsUrl' => $shnCfg['dsUrl'], 'table' => $table];
				file_put_contents($settingsFileName, json_encode($settingsData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
			}
		}

//		$uploadString = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
//		$uploadFileName = time().'-'.$type.'-'.mt_rand(1000000,9999999).'-'.md5($uploadString).'.json';
//		file_put_contents($dir.'/'.$uploadFileName, $uploadString);
	}

	public function saveUploadInfo ($type, $data)
	{
		$dir = '/var/lib/shipard-node/upload/'.$type;
/*		if (!is_dir($dir))
		{
			mkdir($dir, 0770, TRUE);
		}

		$settingsFileName = $dir.'/.settings';
		if (!is_file($settingsFileName))
		{
			$settingsData = ['dsUrl' => $this->app->serverCfg['dsUrl'], 'table' => 'mac.lan.lans'];
			file_put_contents($settingsFileName, json_encode($settingsData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
		}
*/
		$uploadString = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
		$uploadFileName = time().'-'.$type.'-'.mt_rand(1000000,9999999).'-'.md5($uploadString).'.json';
		file_put_contents($dir.'/'.$uploadFileName, $uploadString);
	}

	public function superuser ()
	{
		return (0 == posix_getuid());
	}

	public function run ()
	{
		echo "nothing to do...\r\n";
		return TRUE;
	}
}

function parseArgs($argv)
{
	// http://pwfisher.com/nucleus/index.php?itemid=45
	array_shift ($argv);
	$out = array();
	foreach ($argv as $arg){
		if (substr($arg,0,2) == '--'){
			$eqPos = strpos($arg,'=');
			if ($eqPos === false){
				$key = substr($arg,2);
				$out[$key] = isset($out[$key]) ? $out[$key] : true;
			} else {
				$key = substr($arg,2,$eqPos-2);
				$out[$key] = substr($arg,$eqPos+1);
			}
		} else if (substr($arg,0,1) == '-'){
			if (substr($arg,2,1) == '='){
				$key = substr($arg,1,1);
				$out[$key] = substr($arg,3);
			} else {
				$chars = str_split(substr($arg,1));
				foreach ($chars as $char){
					$key = $char;
					$out[$key] = isset($out[$key]) ? $out[$key] : true;
				}
			}
		} else {
			$out[] = $arg;
		}
	}
	return $out;
}

