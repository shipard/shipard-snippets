<?php

define ('__SNIPPETS_DIR__', '/opt/shipard-snippets');


function __autoload_libs ($class_name)
{
	if (substr ($class_name, 0, 4) === "lib\\")
	{
		$fnparts = explode ("\\", substr ($class_name, 4));

		if (count($fnparts) === 1)
			$fn = __SNIPPETS_DIR__.'/cli-tools/lib/'.$fnparts[0].'.php';
		else
		{
			$cbn = array_pop($fnparts);
			$fn = __SNIPPETS_DIR__.'/cli-tools/lib/'.implode('/', $fnparts).'/'.$cbn.'.php';
		}

		if (is_file ($fn))
			include_once ($fn);
		else
			error_log ('file not found: ' . $fn . ' (required for class ' . $class_name . ')');

		return;
	}
}

spl_autoload_register('__autoload_libs');

