#!/usr/bin/env php
<?php

require_once __DIR__.'/../../lib/boot.php';


/**
 * Class App
 */
class App extends \lib\Application
{
	var $menuData = [];
	var $importUploadId = 'import-canteen-menu';

	function initData()
	{
		$this->menuData = ['canteen' => 1, 'foods' => []];
		$this->initUploadInfo ($this->importUploadId, 'e10pro.canteen.menus');
	}

	function importMenuTxt ($txtFileName)
	{
		$days = ['PONDĚLÍ', 'ÚTERÝ', 'STŘEDA', 'ČTVRTEK', 'PÁTEK'];

		$this->initData();

		$txt = file_get_contents($txtFileName);
		if (!$txt)
			return $this->err("Invalid text file `$txtFileName`");

		$rows = explode("\n", $txt);

		while(1)
		{
			$row = array_shift($rows);
			if ($row === NULL)
				break;

			$row = trim($row);
			if (in_array($row, $days))
			{
				$this->importMenuTxt_OneDay($rows);
			}

			//echo " - $row \n";
		}

		//print_r($this->menuData);

		return TRUE;
	}

	function importMenuTxt_OneDay (&$rows)
	{
		$dateTxt = array_shift($rows);
		if ($dateTxt === NULL)
			return;

		$today = new \DateTime();
		$todayYear = intval($today->format('Y'));

		$dateParts = explode ('.', $dateTxt);
		$day = intval($dateParts[0]);
		$month = intval($dateParts[1]);
		$date = sprintf('%04d-%02d-%02d', $todayYear, $month, $day);

		for ($foodIndex = 1; $foodIndex <= 3; $foodIndex++)
		{
			$soupName = array_shift($rows);
			if (strlen($soupName) < 3)
				$soupName = array_shift($rows);
			$foodName = array_shift($rows);

			//echo "--- $date !$foodIndex! - $soupName - $foodName \n";

			if ($soupName === NULL || $foodName === NULL)
				return;

			$soupName = trim ($soupName);
			$foodName = trim ($foodName);

			$a = [];
			$allergensNumbers = [];
			preg_match("/\([\,\d ]+\)$/", $foodName, $a);
			if (!count($a))
			{
				$allergensOnNextRow = array_shift($rows);
				if ($allergensOnNextRow !== NULL)
				{
					$foodName .= ' ' . trim($allergensOnNextRow);
					preg_match("/\([\,\d ]+\)$/", $foodName, $a);
				}
			}

			if (count($a))
			{
				$lastIdx = count($a) - 1;
				$allergensTxt = $a[$lastIdx];
				$allergens = explode (',', $allergensTxt);
				foreach ($allergens as $aa)
				{
					$aaNumber = intval(preg_replace("/[^0-9]/", '', $aa));
					$allergensNumbers[] = $aaNumber;
				}
				$foodName = trim(substr($foodName, 0, -strlen($allergensTxt)));
				//echo "AAA: ".json_encode($a)."\n";
			}

			$foodItem = ['date' => $date, 'foodIndex' => $foodIndex, 'foodName' => $foodName, 'soupName' => $soupName, 'allergens' => $allergensNumbers];
			$this->menuData['foods'][] = $foodItem;
		}
	}

	function importMenuWord ($fn = '')
	{
		if ($fn !== '')
			$wordFileName = $fn;
		else
			$wordFileName = $this->arg('file');

		if (!$wordFileName)
			return $this->err('Missing --file param');

		$txtFileName = $wordFileName.'.txt';
		$cmd = "docx2txt \"{$wordFileName}\" \"$txtFileName\"";
		passthru($cmd);

		$this->importMenuTxt($txtFileName);


		$this->saveUploadInfo ($this->importUploadId, $this->menuData);

		return TRUE;
	}

	function importMenu()
	{
	}

	public function run ()
	{
		if (!$this->superuser())
			return $this->err ('Need to be root');

		switch ($this->command ())
		{
			case	'import-menu-word':	return $this->importMenuWord();
		}
		echo ("unknown or nothing param....\r\n");

		return parent::run();
	}
}

$myApp = new App ($argv);
$myApp->run ();
